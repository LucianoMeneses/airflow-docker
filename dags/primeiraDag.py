from airflow import DAG
from airflow.operators.python import PythonOperator, BranchPythonOperator 
from datatime import datatime, timedelta
from airflow.operators.bash import BashOperator
from airflow.sensors.filesystem import FileSensor

default_args = {
    'retry': 5,
    'retry_interval': timedelta
}

def _baixando_dados(**kwargs):
    with open('/tmp/meu_arquivo.txt', 'w') as f:
        f.write('meus dados')

with DAG(dag_id='primeiraDag', scheduleInterval=timedelta(minutes=30), start_date=datatime)

     baixando_dados = PythonOperator(
        task_id = 'baixando_dados',
        python_callable = _baixando_dados
     )

      esperando_dados = FileSensor(
        task_id = 'esperando_dados',
        fs_conn_id = 'dados_do_temp',
        filepath='meu_arquivo.txt'
     )

      processando_dados = BashOperator(
        task_id = 'processando_dados',
        bash_command = 'exit 0'
     )

     saida_de_dados = BashOperator(
        task_id = 'Saida de dados',
        bash_command = 'echo Finalizado com sucesso'
     )

